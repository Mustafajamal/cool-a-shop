<?php
/**
 * Plugin Name:       Frame Designer Tool
 * Plugin URI:        logicsbuffer.com/
 * Description:       Sexy Beast Tool [frametool]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       frame-tool
 * Domain Path:       /languages
 */


add_action( 'init', 'custom_frame_designer' );

function custom_frame_designer() {

	//add_shortcode( 'show_custom_fancy_product', 'custom_fancy_product_form' );
	add_shortcode( 'frametool', 'custom_framedesigner_form_single_chop' );
	add_action( 'wp_enqueue_scripts', 'custom_frame_designer_script' );
	//add_action( 'wp_ajax_nopriv_post_love_calculateQuote', 'post_love_calculateQuote' );
	//add_action( 'wp_ajax_post_love_calculateQuote', 'post_love_calculateQuote' );
	// Setup Ajax action hook
	add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_read_me_later',  'read_me_later' );
	add_action( 'wp_ajax_nopriv_read_me_later', 'read_me_later' );

	//wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
	//wp_enqueue_script( 'my_voter_script' );

}

function custom_frame_designer_script() {
		        
	wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/framedesigner/js/mainscript.js',array(),time());	
	wp_enqueue_script( 'rt_custom_fancy_font_observer', plugins_url().'/framedesigner/js/fontfaceobserver.js',array(),time());	
	
	//wp_enqueue_script( 'rt_fabric_script', plugins_url().'/framedesigner/js/fabric.js');
	wp_enqueue_script( 'rt_fabric_script', plugins_url().'/framedesigner/js/fabric2-4.min.js');

	wp_enqueue_script( 'rt_lightbox_script', plugins_url().'/framedesigner/js/lightbox.js');
	wp_enqueue_script( 'rt_jquery_ui', plugins_url().'/framedesigner/js/jquery-ui.js');
	wp_enqueue_script( 'rt_canvas_toblob', plugins_url().'/framedesigner/js/canvas-toBlob.js');
	wp_enqueue_script( 'rt_html2canvas', plugins_url().'/framedesigner/js/html2canvas.js');
	wp_enqueue_script( 'rt_canvas_toblob', plugins_url().'/framedesigner/js/FileSaver.min.js');
	wp_enqueue_script( 'rt_canvas_customizecontrols', plugins_url().'/framedesigner/js/customiseControls.min.js');
	//wp_enqueue_script( 'rt_fabric_script_angular', plugins_url().'/framedesigner/js/angular.min.js',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_style', plugins_url().'/framedesigner/css/mainstyle.css',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_style_responsive', plugins_url().'/framedesigner/css/custom_fancy_responsive.css',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_jqeury_ui', plugins_url().'/framedesigner/css/jquery-ui.min.css',array(),time());
	wp_enqueue_style( 'rt_custom_lightbox_ui', plugins_url().'/framedesigner/css/lightbox.css',array(),time());
}

function custom_framedesigner_form_single_chop() {
	$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
	global $post;
	global $woocommerce;
	//$product_id = $post->ID;

	if(isset($_POST['submit_prod'])){
			
		// //$total_qty = $_POST['rt_qty'];			 
		$quantity = 1;			 
		$product_id = $post->ID;
		$rt_total_price = $_REQUEST['total_price'];
		//$selected_size = $_REQUEST['select_size'];
		$canvas_export = $_REQUEST['canvas_export'];
		//echo $rt_total_price;
		//$woocommerce->cart->add_to_cart( $product_id, $quantity ); 
		update_post_meta($product_id, 'total_price', $rt_total_price);
		//update_post_meta($product_id, 'selected_size', $selected_size);
		update_post_meta($product_id, 'canvas_export', $canvas_export);
		// $rt_total_price = $_POST['pricetotal_quote'];
		// echo $rt_total_price;
		// echo $product_id;
		   
		//Set price
		$product_id = $post->ID;    
	    //$myPrice = get_post_meta('total_price_quote');
	    $myPrice = $rt_total_price;

	    // Get the WC_Product Object instance
		$product = wc_get_product( $product_id );
		$variation_id = "";
		$variation = "";

		// Set the product active price (regular)
		//$product->set_price( $rt_total_price );
		//$product->set_regular_price( $rt_total_price ); // To be sure

		// Save product data (sync data and refresh caches)
		//$product->save();
		//Set price end

		//die();
		// Cart item data to send & save in order
		$cart_item_data = array('custom_price' => $rt_total_price);   
		// woocommerce function to add product into cart check its documentation also 
		// what we need here is only $product_id & $cart_item_data other can be default.
		WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data);
		// // Calculate totals
		WC()->cart->calculate_totals();
		// // Save cart to session
		WC()->cart->set_session();
		// // Maybe set cart cookies
		WC()->cart->maybe_set_cart_cookies();	    

	}
	ob_start();
	//$page_title = get_the_title();
	//$terms = get_the_terms( get_the_ID(), 'product_cat' );
	//$product_type = $terms[0]->slug;
	?>
	<div class="tool-container">
		<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						

			<div id="choptoolmain" class="custom_calculator single_product">

				<img style="display: none;" id="canvastoBlob" src="">
				<input type="hidden" style="display: none;" id="canvas_export" value="" name="canvas_export">
				<input type="button" style="display: none;" id="download_canvas" value="Download canvas">
				<canvas id="canvas_ex" style="border:2px solid red;" width="1100" height="350"></canvas>

				<!-- New Layout -->
				<div class="single_product_tool_n" >	   
				    <div class="funplate_design">
						<input style="" type="hidden" value="repeat" id="repeat">
					</div>
				    
				</div>	
			    <div class="priceandcart">
					<div class="row" id="">
						<div class="col col-4">
							<div class="col-inner">
								<div class="add-cart-btn-main">					
									<input type="submit" name="submit_prod" class="add_to_cart_btn tool-buttons-text" value="Add to Cart" />
									<span class="tool-buttons-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"><metadata><x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01"><rdf:rdf xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"><rdf:description rdf:about=""></rdf:description></rdf:rdf></x:xmpmeta></metadata>
									<defs><filter id="filter" x="884" y="987" width="24" height="24" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"></feFlood><feComposite result="composite" operator="in" in2="SourceGraphic"></feComposite><feBlend result="blend" in2="SourceGraphic"></feBlend></filter></defs><path id="Forma_1_copy" data-name="Forma 1 copy" class="cls-1" d="M907.79,992.453a0.934,0.934,0,0,0-.728-0.346H890.081l-0.069-.64,0-.021A5.129,5.129,0,0,0,884.937,987a0.938,0.938,0,0,0,0,1.875,3.249,3.249,0,0,1,3.212,2.809l1.114,10.239a2.809,2.809,0,0,0-1.654,2.56v0.05a2.814,2.814,0,0,0,2.813,2.81H890.8a2.766,2.766,0,1,0,5.237,0h4.045a2.742,2.742,0,0,0-.148.89,2.766,2.766,0,1,0,2.766-2.76H890.422a0.939,0.939,0,0,1-.938-0.94v-0.05a0.939,0.939,0,0,1,.938-0.94H902.39a4.3,4.3,0,0,0,3.891-2.62,0.937,0.937,0,1,0-1.717-.75,2.446,2.446,0,0,1-2.174,1.5H891.122l-0.837-7.689h15.624l-0.459,2.2a0.939,0.939,0,0,0,.727,1.11,0.969,0.969,0,0,0,.192.019,0.938,0.938,0,0,0,.917-0.746l0.694-3.328A0.936,0.936,0,0,0,907.79,992.453ZM902.7,1007.34a0.89,0.89,0,1,1-.891.89A0.892,0.892,0,0,1,902.7,1007.34Zm-9.281,0a0.89,0.89,0,1,1-.891.89A0.891,0.891,0,0,1,893.422,1007.34Z" transform="translate(-884 -987)"></path>
									</svg></span>
								</div>		
							</div>		
						</div>
						<div id="" class="col col-4" style="display: none;">
							<?php
								// $product_id = $post->ID;
								// $product = wc_get_product( $product_id );
								// $product_price_woo = $product->get_regular_price();
								// $background = get_field("background");
								// $container_background_color = get_field("container_background_color");
								// //$use_image_background = get_field("use_image_background");
								
								// $field = get_field_object("use_image_background");
								// $use_image_background = $field['value'];
								
								// //if( in_array('Yes', $use_image_background) ) {								
								// 	//echo "<input type='hidden' name='use_image_background' id='use_image_background' value='Yes'>";								
								// //}
								// // if( in_array('No', $use_image_background) ) {								
								// // 	echo "<input type='hidden' name='use_image_background' id='use_image_background' value='No'>";								
								// // }

								// $container_background_image = get_field("container_background_image");
							?>
							<!--<div class="col-inner">
								<div class="icon-box featured-box total_nis_btn icon-box-right text-right">
									<div class="icon-box-text last-reset">									
										<div id="text-857485485" class="text">
										<input type="hidden" name="total_price_default" id="total_price_default" value="<?php //echo $product_price_woo;?>">		
										<input type="hidden" name="total_price" id="total_price" value="<?php //echo $product_price_woo;?>">		
										<input type="hidden" name="background" id="background" value="<?php //echo $background;?>">		
										<input type="hidden" name="container_background_color" id="container_background_color" value="<?php //echo $container_background_color;?>">	
										<input type="hidden" name="use_image_background" id="use_image_background" value="<?php //echo $use_image_background; ?>">
										<input type="hidden" name="container_background_image" id="container_background_image" value="<?php //echo $container_background_image;?>">		
										<h3>Total <span id="total_price"><?php //echo $product_price_woo;?></span></h3>		
										</div>
									</div>
								</div>
							</div>-->
							
						</div>
					</div>
				</div>
		</form>
	</div>

	<?php 
		$contents = ob_get_contents();
		ob_end_clean();
		return $contents;
	}
	/**
	 * Add engraving text to cart item.
	 *
	 * @param array $cart_item_data
	 * @param int   $product_id
	 * @param int   $variation_id
	 *
	 * @return array
	 */
	function iconic_add_engraving_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
		$engraving_text = filter_input( INPUT_POST, 'canvas_export' );
		$engraving_text = filter_input( INPUT_POST, 'ywapo_text_5[0]' );

		if ( empty( $engraving_text ) ) {
			return $cart_item_data;
		}

		$cart_item_data['iconic-engraving'] = $engraving_text;

		return $cart_item_data;
	}

	add_filter( 'woocommerce_add_cart_item_data', 'iconic_add_engraving_text_to_cart_item', 10, 3 );

	/**
	 * Display engraving text in the cart.
	 *
	 * @param array $item_data
	 * @param array $cart_item
	 *
	 * @return array
	 */
	function iconic_display_engraving_text_cart( $item_data, $cart_item ) {
		if ( empty( $cart_item['iconic-engraving'] ) ) {
			return $item_data;
		}
		//'display' => '<img src="'.$cart_item['iconic-engraving'].'">',
		$base64_img = $cart_item['iconic-engraving'];
		
		//Upload to media library
		// Upload dir.
		$title = "customized";
		$upload_dir  = wp_upload_dir();
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

		$img             = str_replace( 'data:image/png;base64,', '', $base64_img );
		$img             = str_replace( ' ', '+', $img );
		$decoded         = base64_decode( $img );
		$filename        = $title . '.png';
		$file_type       = 'image/png';
		$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

		// Save the image in the uploads directory.
		$upload_file = file_put_contents( $upload_path . $hashed_filename, $decoded );

		$attachment = array(
			'post_mime_type' => $file_type,
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $hashed_filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
			'guid'           => $upload_dir['url'] . '/' . basename( $hashed_filename )
		);

		$attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $hashed_filename );
		//return $attach_id;
		$uploaded_url = wp_get_attachment_url( $attach_id );

		$item_data[] = array(
			'key'     => __( 'Engraving', 'iconic' ),
			'value'   => wc_clean( $cart_item['iconic-engraving'] ),
			'display' => '<img src="'.$uploaded_url.'">'
		);

		return $item_data;
	}

	add_filter( 'woocommerce_get_item_data', 'iconic_display_engraving_text_cart', 10, 2 );

	/**
	 * Add engraving text to order.
	 *
	 * @param WC_Order_Item_Product $item
	 * @param string                $cart_item_key
	 * @param array                 $values
	 * @param WC_Order              $order
	 */
	function iconic_add_engraving_text_to_order_items( $item, $cart_item_key, $values, $order ) {
		if ( empty( $values['iconic-engraving'] ) ) {
			return;
		}

		$item->add_meta_data( __( 'Engraving', 'iconic' ), $values['iconic-engraving'] );
	}

	add_action( 'woocommerce_checkout_create_order_line_item', 'iconic_add_engraving_text_to_order_items', 10, 4 );

	// add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);
	// add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data', 10, 2);
	// 	function order_meta_handler($item_id, $values, $cart_item_key) {
	// 		// Allow plugins to add order item meta

	// 		$cart_session = WC()->session->get('cart');
	// 		// if($cart_session[$cart_item_key]['rt_total_price'][0]){
	// 		// wc_add_order_item_meta($item_id, "Total Price", $cart_session[$cart_item_key]['rt_total_price'][0]);
	// 		// }
	// 		if($cart_session[$cart_item_key]['selected_size'][0]){
	// 			wc_add_order_item_meta($item_id, "Selected Size", $cart_session[$cart_item_key]['selected_size'][0]);
	// 		}
	// 		if($cart_session[$cart_item_key]['canvas_export'][0]){
	// 			wc_add_order_item_meta($item_id, "Thumbnail", $cart_session[$cart_item_key]['canvas_export'][0]);
	// 		}
	// 	}
	// 	function add_cart_item_custom_data($cart_item_meta, $new_post_id) {
	// 		global $woocommerce;
	// 		$product_id = $post->ID;
	// 		//$rt_total_price = get_post_meta($product_id, 'rt_total_price');
	// 		$selected_size = get_post_meta($product_id, 'selected_size');
	// 		$canvas_export = get_post_meta($product_id, 'canvas_export');

	// 		return $cart_item_meta;
	// 	}


	// add_action( 'woocommerce_product_meta_end', 'woocommerce_single_variation_add_to_cart_btn', 20 );
	// function woocommerce_single_variation_add_to_cart_btn(){
	//     echo do_shortcode('[frametool]');
	// }
	//Ajax
	function ajax_enqueue() {
	    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/my-ajax-script.js', array('jquery') );
	    wp_localize_script( 'ajax-script', 'my_ajax_object',
	            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}

	add_action( 'wp_enqueue_scripts', 'ajax_enqueue' );
	add_action( 'wp_ajax_foobar', 'wp_ajax_foobar_handler' );
	add_action( 'wp_ajax_nopriv_foobar', 'wp_ajax_foobar_handler' );
	function wp_ajax_foobar_handler() {

		//'display' => '<img src="'.$cart_item['iconic-engraving'].'">',
		$base64_img = $_REQUEST['canvas_export'];
		
		//Generate rendom serial number
		// $len = 10;   // total number of numbers
		// $min = 100;  // minimum
		// $max = 999;  // maximum
		// $range = []; // initialize array
		// foreach (range(0, $len - 1) as $i) {
		//     while(in_array($num = mt_rand($min, $max), $range));
		//     $range[] = $num;
		// }

		//print_r($range);

		//Upload to media library
		// Upload dir.
		$title = "MF-".date("Y-m-d")."-".uniqid()."";
		$upload_dir  = wp_upload_dir();
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

		$img             = str_replace( 'data:image/png;base64,', '', $base64_img );
		$img             = str_replace( ' ', '+', $img );
		$decoded         = base64_decode( $img );
		$filename        = $title . '.png';
		$file_type       = 'image/png';
		$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

		// Save the image in the uploads directory.
		$upload_file = file_put_contents( $upload_path . $hashed_filename, $decoded );

		$attachment = array(
			'post_mime_type' => $file_type,
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $hashed_filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
			'guid'           => $upload_dir['url'] . '/' . basename( $hashed_filename )
		);

		$attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $hashed_filename );
		//return $attach_id;
		$uploaded_url = wp_get_attachment_url( $attach_id );

		echo $uploaded_url;
		wp_die();
	}