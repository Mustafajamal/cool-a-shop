// New Angular code
jQuery(document).ready(function() {

	//jQuery("#zoomplus,#zoomminus,#angle_plus,#angle_minus").prop('disabled', true);
	// jQuery("#remove-color-distance").slider({
	// 	range: "min",
	// 	value: 0.82,
	// 	min: 0,
	// 	max: 1,
	// 	step: 0.01,
	// 	animate: 200,
	// 	slide: function(event, ui) {
	// 		console.log("Remove Color Distance"+ui.value);
	// 		applyFilterValue(2, 'distance', ui.value);
	// 	}
	// }); 

	// var canvas = new fabric.Canvas('canvas',{
	// 	preserveObjectStacking: true,
	// });
	// shadow: new fabric.Shadow({
 //      color: "rgb(26, 25, 25)",
 //      blur: 1,
 //      offsetX: -4,
 //      offsetY: 3
 //    })
// fill: '#716c6c',
// stroke: '##aaa8a8',
// strokeWidth: 5,
// paintFirst: 'stroke', // stroke behind fill

	  var canvas = this.__canvas = new fabric.Canvas('canvas_ex');
	  fabric.Object.prototype.transparentCorners = false;

		var text = new fabric.Textbox("Text", {
			id: "fp-text",
			width: 750,
			fontFamily: 'Helvetica Neue',
			fontSize: 120,
			fontWeight: 600,
			charSpacing: 1,
			text: "SEXY BEAST",
			shadow: new fabric.Shadow({
		      color: "#2b2a2a",
		      blur: 1,
		      offsetX: -3,
		      offsetY: -2
		    })	    
		});
		var shape = new fabric.Rect({
			width: 800,
			height: 170,
			left: 10,
			top: 300,
			rx: 15,
		    ry: 15,
		    objectCaching: false,
		});
		
		//Line
		var line = new fabric.Rect({
            width:790,
            height:160,
            stroke:'red',
            strokeWidth:5,
            rx: 15,
		    ry: 15,
            fill:'transparent'
        });

		canvas.add(text, shape, line);
		canvas.setActiveObject(text);
		canvas.bringForward(text);
		canvas.bringForward(line);
		canvas.sendToBack(shape);
		canvas.centerObject(shape);
		canvas.centerObject(text);
		canvas.centerObject(line);

	    jQuery('#fp-text').on('keyup', function() {
		  id = jQuery(this).attr('id');
		  val = jQuery(this).attr('data-text');
		  newtext = jQuery(this).val();
		  input = jQuery(this);

		  objs = canvas.getObjects();
		  objs.forEach(function(obj) {
		    if (obj && obj.text == val) {
		      //obj.setText(newtext);
		      obj.set({
			    text : newtext
			  });
		      input.attr("data-text", newtext);
		      canvas.renderAll();
		    }
		  });
		});

		function loadPatternText(url) {
		    fabric.util.loadImage(url, function(img) {
		      text.set('fill', new fabric.Pattern({
		        source: img,
		        repeat: document.getElementById('repeat').value
		      }));
		      canvas.renderAll();
		    });
		  }
		  function loadPatternLine(url) {
		    fabric.util.loadImage(url, function(img) {
		      line.set('stroke', new fabric.Pattern({
		        source: img,
		        repeat: document.getElementById('repeat').value
		      }));
		      canvas.renderAll();
		    });
		    localStorage.setItem('currentpattern',url);
		  }
		  //Icon
		  // function loadPatternIcon(url) {
		  //     fabric.util.loadImage(url, function(img) {
		  //     line.set('fill', new fabric.Pattern({
		  //       source: img,
		  //       repeat: document.getElementById('repeat').value
		  //     }));
		  //     canvas.renderAll();
		  //   });
		  // }
		  function loadPatternBg(url) {
		    fabric.util.loadImage(url, function(img) {
		      shape.set('fill', new fabric.Pattern({
		        source: img,
		        repeat: document.getElementById('repeat').value
		      }));
		      canvas.renderAll();
		    });
		}

	  loadPatternText('https://coolashoppen.com/wp-content/uploads/2021/11/thumb_pattern_silver_glitter-1.jpg');
	  loadPatternBg('https://coolashoppen.com/wp-content/uploads/2021/11/thumb_plate_red.jpg');
	  loadPatternLine('https://coolashoppen.com/wp-content/uploads/2021/11/thumb_pattern_silver_glitter.jpg');

	  //Load Text Pattern
	  jQuery('#text-patterns li img').click(function(){
	    loadPatternText(jQuery(this).attr('src'));
	    loadPatternLine(jQuery(this).attr('src'));
	    var selsvg = localStorage.setItem("selsvg");
	    iconPattern(selsvg);
	  });
	  
	    //Add Icon
	    jQuery('.mainicons li img').click(function(){
		   //   var selected_icon = jQuery(this).attr('data-icon');
		   //   var $txt = jQuery("#fp-text");
	   	   //   var caretPos = $txt[0].selectionStart;
	   	   //   var textAreaTxt = $txt.val();
	   	   //   var txtToAdd = selected_icon;
	   	   //   $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
	   	   var selsvg = jQuery(this).attr('datasvg');
	   	   iconPattern(selsvg);
	   	   localStorage.setItem("selsvg",selsvg);
	    });
	  	
	    function iconPattern(selsvg){
	    	//Adding svg image with selected pattern

			var patternURL = localStorage.getItem('currentpattern');
			//var SVGURL = "https://coolashoppen.com/wp-content/uploads/2021/11/thumb_symbol_radioactive_1458338266-1.svg";
			var SVGURL = selsvg;
			
		    fabric.loadSVGFromURL(SVGURL, function (objects, options) {
	        var oSvg = fabric.util.groupSVGElements(objects, options);
	        oSvg.set({left: 180, top: 150}).scale(1);

		        fabric.Image.fromURL(patternURL, function (img) {
		            var patternSourceCanvas = new fabric.StaticCanvas();
		            patternSourceCanvas.setDimensions({
		                width: img.getScaledWidth()/1,
		                height: img.getScaledHeight()/1
		            });
		            patternSourceCanvas.add(img);
		            var pattern = new fabric.Pattern({
		                source: patternSourceCanvas.getElement(),
		                repeat: 'repeat'
		            });
		            console.log(oSvg._objects);
		            if (oSvg._objects == "undefined") {
		            	oSvg.fill = pattern;
		            }
		            oSvg.getObjects().forEach(function (e) {
		                e.fill = pattern
		            });
		            
		            canvas.add(oSvg);
		            pattern.repeat = 'repeat';
		            oSvg.dirty = true;
                	canvas.requestRenderAll();
		        });    
	        });    

		 	text.set({ width: 610, top: 120,fontSize: 96, left: 270 });
	    }

	  
	  //Load Bg Pattern
	  jQuery('#bg-patterns li img').click(function(){
	    loadPatternBg(jQuery(this).attr('src'));
	  });

	  //document.getElementById('repeat').onchange = function() {
	    //text.fill.repeat = this.value;
	    //text.fill.repeat = 'repeat';
	    canvas.renderAll();
	  //};

	  	//Fonts
	  	// var fonts = ["AmaticSC-Bold", "AnkaCLM-Bold", "MINIMALIST_1_", "Nehama", "SuezOne-Regular"];
    // 	//$.each(fonts, function(index, value){
    // 	for(var i = 0 ; i < fonts.length; i++){
    // 		jQuery(".fonts_ul").append("<div class='fonts' style='font-family:"+ fonts[i] +"' data-val="+ fonts[i] +">"+ fonts[i] +"</div>");
    // 	}

	 //    function loadAndUse(font) {
	 //    	var myfont = new FontFaceObserver(font)
	 //    	myfont.load()
	 //    	.then(function() {
	 //          // when font is loaded, use it.
	 //          canvas.getActiveObject().set("fontFamily", font);
	 //          canvas.getActiveObject().set("charSpacing", 1);
	 //          canvas.getActiveObject().set("fontSize", 400);
	 //          canvas.getActiveObject().set("fontWeight", 200);
	 //          canvas.requestRenderAll();
	 //      	}).catch(function(e) {
	 //      	console.log(e)
	 //          alert('font loading failed ' + font);
	 //      	});
		// }
		// loadAndUse('Revelstoke'); 
 
	//angle controllers
	// jQuery("#angle_plus").click(function(){
	//   var curAngle = canvas._activeObject.angle;
	//   canvas._activeObject.angle = (curAngle+1);
	//   jQuery("#angleinput").val(canvas._activeObject.angle); 
	//   canvas.renderAll();
	// });	




	//Images Uploaded Click Option
	// jQuery(document).on('click', '.upload-image-content img', function() {
	// 	console.log("img click...");
	// 	var src = jQuery(this).attr('src');
	// 	loadImage(src);
	// 	jQuery(".upload-image-button.tool-buttons.main-actions,.upload-image-content").hide();	
	// });
	// canvas.on('object:selected', function(event) {
	//     var object = event.target;
	//     //canvas.sendToBack(object);	    
	//     object.sendBackwards();
	//     console.log("Selected");
	// });	
		// fabric.util.addListener(document.getElementById('icon_right'), 'click', function () {    		
		// 	//var boundingbox = localStorage.getItem("boundingbox");
		//     var obj = canvas.getActiveObject();	    
		//     console.log(obj.getScaledWidth());
		//     obj.set({
		//     	left: boundingbox.left + boundingbox.width - obj.getScaledWidth(),
		//     	selectable: true,
		//     	hasBorders: true,
		//     	hasRotatingPoint: false
		//     });
		//     canvas.setActiveObject(obj);
		//     canvas.renderAll();				
		// });
	//});

	//Default Size and Side B Price
	// var total_price_default = jQuery("#total_price_default").val();
	// var side_b_price = document.getElementById('side_b_price').value; 
	// total_price = parseFloat(40) + parseFloat(side_b_price) + parseFloat(total_price_default);
	// jQuery("#total_price").val(total_price);
	// jQuery(".product-price-container .price .amount:last").html(total_price);
	// jQuery("h3 span#total_price").html(total_price);

	// //Set Selected Size Price on selection
	// jQuery('input[name="select_size"]').on('change', function() {
	// 	var total_price_default = jQuery("#total_price_default").val();
	// 	var selected_size = jQuery(".selsize_value input[type='radio']:checked").val();
	// 	var side_b_price = document.getElementById('side_b_price').value;

	// 	//console.log("selected_size.."+selected_size);
	// 	if (selected_size == "30-40"){
	// 		total_price_size = parseFloat(total_price_default) + parseFloat(40);
	// 	}
	// 	else if (selected_size == "32-22"){
	// 		total_price_size = parseFloat(total_price_default) + parseFloat(50);
	// 	}
	// 	//Adding Side b Price
	// 	total_price_modified = parseFloat(total_price_size) + parseFloat(side_b_price);

	// 	jQuery("#total_price").val(total_price_modified);
	// 	jQuery(".product-price-container .price .amount:last").html(total_price_modified);
	// 	jQuery("h3 span#total_price").html(total_price_modified);
	// });
  // jQuery(".add_to_cart_btn").click(function(){
  //   var total_price = jQuery("#total_price").html();
  //   jQuery.ajax({
  //     url : ajaxurl,
  //     type : 'POST',
  //     data: {
  //       action: 'read_me_later',
  //       total_price : total_price
  //           },
  //     success : function( response ) {
  //       //url = response;
  //       //jQuery("#rt_waiting").css({"display": "none"});
  //       console.log("total_price....Ajax.."+total_price);
  //       //window.open(url,"_self");
  //     },
  //     error: function(e) {
  //         console.log(e);
  //     }
  //   });
  // });
 
 	//Canvas Export
	//jQuery( "#download_canvas" ).click(function() {
		
		// var dataURL = canvas.toDataURL({
		//   format: 'png',
		//   left: 10,
		//   top: 10,
		//   width: 450,
		//   height: 460
		// }); 
		// console.log(dataURL);
		// jQuery("#canvastoBlob").attr('src',dataURL);
		// jQuery("#canvas_url").val(dataURL);

		// html2canvas(document.querySelector("#choptoolmain"),{
		// 	allowTaint: true,
		// 	canvas: null
		// }).then(
		// 	canvas => {
		//     document.body.appendChild(canvas)
		// 	}
		// );
		// window.open(canvas.toDataURL('png'));
		
		// html2canvas(document.getElementById('choptoolmain'), {
	 //        allowTaint:true,
	 //        useCORS:true,
	 //        proxy:"lib/html2canvas_proxy/html2canvasproxy.php",
	 //        onrendered: function(canvas) {
	 //            var result = canvas.toDataURL();
	 //            console.log("result.."+result);
	 //        }
	 //   }).then(
		// 	canvas => {
		//     document.body.appendChild(canvas)
		// 	}
		// );


	//}); 
	// jQuery( "#download_canvas" ).click(function() {
	//   	function saveImage(e) {
	// 	    this.href = canvas1.toDataURL({
	// 	        format: 'png',
	// 	        quality: 0.8
	// 	    });
	// 	    this.download = 'design.png'
	// 	}
	// });
  //Export Fabric Canvas to Image
  // fabric.Image.fromURL('https://via.placeholder.com/350x150', function(img){
  //   img.setWidth(200);
  //   img.setHeight(200);
  //   canvas.add(img);
  // }); 
  //const client = filestack.init('AeAn2tWnEREO3FZeI6R9yz');
  //client.picker().open();

	// jQuery(".add_to_cart_btn.tool-buttons-text").click(function(){
 //    //jQuery(".icon-box.featured-box.total_nis_btn.icon-box-right.text-right").click(function(){
 //    	jQuery("#canvas").get(0).toBlob(function(blob){
 //      saveAs(blob, "myIMG.png");
 //      console.log(blob);
      
 //      var reader = new FileReader();
 //      reader.readAsDataURL(blob); 
 //      reader.onloadend = function() {
 //      	var base64data = reader.result;                
 //      	console.log(base64data);
 //      	jQuery("#canvastoBlob").attr("src", base64data);
 //      	jQuery("#canvas_export").val(base64data);
 //      }
 //  	});    
 //  });    

    //}); 

	 //    //Load Uploaded Image into Canvas
		//  //var src1 = "https://cdn.filestackcontent.com/889uTzOERFujn9CAUmnd";
		//  var uploaded_file = localStorage.getItem("uploaded_file");
		//  //loadImage(uploaded_file);
		//  jQuery(".upload-image-button").click(function(){
		// 		const apikey = 'AYGP0ayqQSLSakKdifkwhz';
		// 		const client = filestack.init(apikey);
		// 		const options = {
		// 			maxFiles: 20,
		// 			maxSize: 3000000,
		// 			uploadInBackground: false,
		// 			onOpen: () => console.log('opened!'),
		// 			onUploadDone: (res) => addtocanvas(res),
		// 		};
		// 	//localStorage.setItem("uploaded_file", res);
		// 	client.picker(options).open();
		// });   
	
		// //Background Color and Image 
		// var use_image_background = jQuery("#use_image_background").val();
		// console.log("use_image_background.."+use_image_background);
		// if (use_image_background == "Yes") {
		// 	var container_background_image = jQuery("#container_background_image").val();
	 //   	console.log("container_background_image.."+container_background_image);
	 //   	jQuery(".canvas-container").css('background-image', 'url(' + container_background_image + ')');
		// }else{
		// 	var container_background_color = jQuery("#container_background_color").val();
	 //   	jQuery(".canvas-container").css("background", container_background_color);
  //   	}
		// function addtocanvas(res){
		// 	//console.log(res);
		// 	console.log("Uploaded File.."+res.filesUploaded[0].url);
		// 	console.log("Uploaded FileName.."+res.filesUploaded[0].filename);
		// 	console.log("Uploaded Key.."+res.filesUploaded[0].Key);
		// 	console.log("Uploaded Mime Type.."+res.filesUploaded[0].mimetype);
		// 	//localStorage.setItem("uploaded_file", res.filesUploaded[0].url);
		// 	var uploaded_file = res.filesUploaded[0].url;
		// 	loadImage(uploaded_file);
		// 	jQuery(".main-actions-parent").hide();
		// 	// jQuery.post(
  //  //              my_ajax_object.ajax_url, 
  //  //              {
  //  //                  'action': 'foobar',
  //  //                  'clicked_image': uploaded_file
  //  //              }, 
  //  //              function(response) {
  //  //                  console.log('The server responded: ', response);
  //  //                  jQuery("#preview").attr("src","data:image/png;base64,"+response);
  //  //              }
  //  //          );
		// jQuery("#zoomplus,#zoomminus,#angle_plus,#angle_minus").prop('disabled', false);
		// }

});